function switchTabs() {

    const tabsContainer = document.querySelector(".tabs");
    let tabs = document.querySelectorAll(".tabs-title");
      let contents = document.querySelectorAll(".tabs-content li");
    
      tabsContainer.addEventListener("click", (event) => {
      
            const clickedTab = event.target.closest(".tabs-title");
    
        if (!clickedTab) return; // Ignore clicks on elements other than tabs
    
      tabs.forEach((tab, index) => {
              if (tab === clickedTab) {
                // Add active class to the clicked tab
                tab.classList.add("active");
                contents[index].style.display = "block";
              } else {
                // Remove active class from other tabs
                tab.classList.remove("active");
                contents[index].style.display = "none";
              }
            });
          });
  
       // Add active class to the first tab and content
      tabs[0].classList.add("active");
      contents[0].style.display = "block";
    }
    
    window.addEventListener("DOMContentLoaded", () => {
      switchTabs();
    });